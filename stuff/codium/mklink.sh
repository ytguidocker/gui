#!/bin/bash
set -o errexit -o pipefail -o nounset
#mypath=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

orig=codium

mv /usr/bin/${orig} /usr/bin/${orig}.original
cat > /usr/bin/${orig} << ==============EOF==============
#!/bin/bash
/usr/bin/${orig}.original --no-sandbox "\$@"
==============EOF==============
chmod +x /usr/bin/${orig}
cp /usr/bin/${orig} /usr/bin/${orig}--no-sandbox
