#!/bin/bash
set -o errexit -o pipefail -o nounset

mypath=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. /etc/os-release

if [[ "ubuntu" == "${ID}" ]]; then
    # Achtung! Variablen werden in den HereDocs ersetzt
    # falls das nicht soll, dann müssen die BeginTags in Quotes.
    cat > /etc/apt/sources.list.d/debian.list << ==============EOF==============
deb https://deb.debian.org/debian stable main
deb https://deb.debian.org/debian stable-updates main
deb https://deb.debian.org/debian-security stable-security main
==============EOF==============

    cat > /etc/apt/preferences.d/chromium.pref << ==============EOF==============
# Note: 2 blank lines are required between entries
Package: *
Pin: release a=eoan
Pin-Priority: 500

Package: *
Pin: origin "deb.debian.org"
Pin-Priority: 300

# Pattern includes 'chromium', 'chromium-browser' and similarly
# named dependencies:
Package: chromium*
Pin: origin "deb.debian.org"
Pin-Priority: 700

Package: libwebpmux3*
Pin: origin "deb.debian.org"
Pin-Priority: 700
==============EOF==============


    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys DCC9EFBF77E11517
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AA8E81B4331F7F50
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 112695A0E562B32A
fi

apt-get update
apt-get install -y chromium
