#!/bin/sh
set -e
project=VSCodium/vscodium

downloadurl=$(curl -s https://api.github.com/repos/${project}/releases/latest \
| grep "browser_download_url.*amd64\.deb\"" \
| cut -d : -f 2,3 \
| tr -d \"
)
curl -s -L --output codium.deb ${downloadurl}
