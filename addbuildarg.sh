#!/bin/sh
set -e

CODIUM_VERSION=$(getLatestGitHubVerNo.sh VSCodium/vscodium)
#curl -s -L https://github.com/VSCodium/vscodium/releases/download/${CODIUM_VERSION}/codium_${CODIUM_VERSION}-1615600425_amd64.deb --output codium.deb
./getLatestCodium.sh


echo "--build-arg CODIUM_VERSION=${CODIUM_VERSION}"
