ARG BASEIMAGE
FROM ${BASEIMAGE} as gui-textonly
#FROM ubuntu:20.04 as gui-textonly

# Set the locale
#RUN apt-get update -q --fix-missing \
RUN apt-get update \
	&& apt-get upgrade -yq \
	&& apt-get install -yq locales tzdata apt-utils \
	&& locale-gen de_DE.UTF-8 en_US.UTF-8
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8
RUN update-locale LANG=de_DE.utf8
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get upgrade -yq \
	&& apt-get install -y -q \
	tmux \
	aptitude \
	nano \
	mc \
	ncdu \
	dnsutils \
	net-tools \
	iputils-ping \
	less \
	telnet \
	rsync \
	sudo \
	htop \
	supervisor \
	python3-pip \
	curl html2text wget \
	par2 unrar-free libffi-dev libssl-dev \
	whois \
	ecryptfs-utils \
	pwgen \
	libtar0 mediainfo \
	tree \
	jq \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

RUN unlink /etc/localtime \
	&& ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

RUN mkdir -p /var/log/supervisor \
			/etc/supervisor/conf.d/


FROM gui-textonly as gui-base
RUN apt-get update && apt-get upgrade -yq \
	&& apt-get install -y -q \
	xorgxrdp xrdp \
	xfce4 xfce4-goodies xfce4-terminal \
	dbus-x11 \
	shiki-colors-xfwm-theme \
	firefox firefox-locale-de firefox-locale-en \
	remmina \
	freerdp2-x11 \
	fonts-hack-ttf \
	xterm \
	clusterssh \
	doublecmd-gtk \
	espeak \
	catfish \
	glogg \
	idle3 \
	flameshot \
	wxhexeditor \
	gvfs-backends \
	soundkonverter \
	atril \
	pdfarranger \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* \
	&& usermod -a -G ssl-cert xrdp

#	xfwm4-themes \
#	chromium-browser \

VOLUME [ "/home" ]
EXPOSE 3389/tcp


FROM gui-base
COPY stuff/ /stuff/
RUN chmod +x /stuff/*.sh

### Codium
ARG CODIUM_VERSION
ENV CODIUM_VERSION ${CODIUM_VERSION}
RUN echo CODIUM_VERSION ${CODIUM_VERSION}
#ADD https://github.com/VSCodium/vscodium/releases/download/${CODIUM_VERSION}/codium_${CODIUM_VERSION}-1615600425_amd64.deb /root/codium.deb
ADD codium.deb /root/codium.deb
RUN dpkg -i /root/codium.deb \
	&& rm /root/codium.deb \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*
RUN chmod +x /stuff/codium/mklink.sh && /stuff/codium/mklink.sh

### Chromiumstuff
RUN chmod +x /stuff/chromium/install.sh && /stuff/chromium/install.sh \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*
RUN chmod +x /stuff/chromium/mklink.sh && /stuff/chromium/mklink.sh


ENTRYPOINT [ "/usr/bin/supervisord", "-c", "/stuff/supervisord.conf" ]
