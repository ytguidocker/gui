#!/usr/bin/env bash
CI_REGISTRY_IMAGE=registry.gitlab.com/ytguidocker/gui

# für die Hand
# docker build --build-arg BASEIMAGE=debian:bookworm-backports -t mygui -f debian/Dockerfile .
# docker compose -f docker-compose-debian.yml up --build
# docker compose -f docker-compose-debian.yml down

function ubuntu() {
	export TARGETOS=ubuntu
	export TARGETOSVERSION="20.04"
}

function debian() {
	export TARGETOS=debian
	#export TARGETOSVERSION="testing"
	#export TARGETOSVERSION="bookworm"
	export TARGETOSVERSION="bookworm-backports"
}

if [ -z $1 ]; then
	echo "enter target os"
	exit 0
fi

$1

export DOCKER_BUILDKIT=0

./build.py --registryimagebase=${CI_REGISTRY_IMAGE} --targetos=${TARGETOS} --targetosversion=${TARGETOSVERSION}  --nopush -v --tag localbuild --tag SHORT_SHA --additional-buildargs "$(./addbuildarg.sh)"
#./build.py --registryimagebase=${CI_REGISTRY_IMAGE} --targetos=${TARGETOS} --targetosversion=${TARGETOSVERSION}  --dryrun --tag localbuild --tag SHORT_SHA --additional-buildargs "$(./addbuildarg.sh)"

mycompose="docker compose --file docker-compose-${TARGETOS}.yml"
${mycompose} up  -d

. ./.env

myport=33890
# l
#myport=33891
# xl
#myport=33892

while true; do
        ret=0
        xfreerdp /u:${guiuser} /p:${guipass} /rfx /sound /v:localhost:${myport} || ret=$?
# +clipboard  /f
        if [ "$ret" -eq "0"  ] ||  [ "$ret" -eq "12"  ]; then break; fi
        sleep 1
done

${mycompose}  down
