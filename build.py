#!/usr/bin/env python3

import os, sys, time, shutil
import functools
from argparse import ArgumentParser
import logging



def main():
    ret = 0
    image = f"{cli.registryimagebase}/{cli.targetos}-{cli.targetosversion}"
    
    buildNpush(baseimage = f"{cli.targetos}:{cli.targetosversion}", 
                   image = image,
             pathcontext = f"{cli.targetos}",
               cacheSelf = False)
             
    buildNpush(baseimage = image, 
                   image = f"{image}-l",
             pathcontext = f"{cli.targetos}/l")
             
    buildNpush(baseimage = f"{image}-l", 
                   image = f"{image}-xl",
             pathcontext = f"{cli.targetos}/xl")
    return ret
             


def buildNpush(baseimage, image, pathcontext, cacheSelf=True):
    shell.run(f"docker pull {image}:latest || true")
    realtags = cli.tags[:] + ["latest"]
    tags = "".join([f"-t {image}:{t} " for t in realtags ])
    selfcache = f"--cache-from {image}:latest" if cacheSelf else ""
    ret = shell.run(f"docker build --compress --build-arg BASEIMAGE={baseimage} {cli.additionalbuildargs} --pull --cache-from {baseimage} {selfcache} {tags} -f {pathcontext}/Dockerfile .")
    if (ret is None or ret == 0) and not cli.nopush:
        for tag in realtags:
            r = shell.run(f"docker push \"{image}:{tag}\"")
            ret = r if r else ret



        






#  ------- ↓↓↓↓↓↓↓ -------  helper  ------- ↓↓↓↓↓↓↓ -------
def init():
    global logger
    global cli
    logging.basicConfig(
        format='[%(asctime)s][%(name)s] %(levelname)s: %(message)s',
        level=logging.INFO,
        handlers=[logging.StreamHandler(sys.stdout)]
    )
    logger = logging.getLogger(__name__)

    parser = ArgumentParser(description="build docker", add_help=False)
    group = parser.add_argument_group("required arguments")
    group.add_argument("-t", "--tag",   dest="tags", action="append", help="Tags, can be entered multiple times", required=True)
    group.add_argument("--registryimagebase", dest="registryimagebase", help="registryimagebase", required=True)
    group.add_argument("--targetos", dest="targetos", help="targetos", required=True)
    group.add_argument("--targetosversion", dest="targetosversion", help="targetosversion", required=True)
    group.add_argument("--additional-buildargs", dest="additionalbuildargs", help="additional buildargs", required=True)

    group = parser.add_argument_group("optional arguments")
    group.add_argument("--nopush",  action="store_true", dest="nopush",  help="build but no push")
    group.add_argument("-d", "--dryrun",  action="store_true", dest="dryrun",  help="only print the action")
    group.add_argument("-v", "--verbose", action="store_true", dest="verbose", help="set loglevel to debug")
    group.add_argument("-h", "--help",    action="help", help="show this help message")
    cli = parser.parse_args()
    if cli.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

class shell:
    class decor:
        @staticmethod
        def __funcfullname(func, *args, **kwargs):
            args_repr = [repr(a) for a in args]                      # 1
            kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
            signature = ", ".join(args_repr + kwargs_repr)           # 3
            return f"{func.__name__}({signature})"

        @classmethod
        def debug(cls, func):
            """Print the function signature and return value"""
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                logger.debug(f"Calling {cls.__funcfullname(func, *args, **kwargs)}")
                value = func(*args, **kwargs)
                logger.debug(f"{func.__name__!r} returned {value!r}")           # 4
                return value
            return wrapper

        @classmethod
        def dryrun(cls, func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                if cli.dryrun:
                    logger.info(f"dryrun: Calling {cls.__funcfullname(func, *args, **kwargs)}")
                else:
                    return func(*args, **kwargs)
            return wrapper


    @staticmethod
    @decor.debug
    @decor.dryrun
    def mkdir(directory, exist_ok=True):
        return os.makedirs(directory, exist_ok=exist_ok)
        
    @staticmethod
    @decor.debug
    @decor.dryrun
    def rmdir(directory):
        return os.rmdir(directory)
        
    @staticmethod
    @decor.debug
    @decor.dryrun
    def mv(src, dest):
        return shutil.move(src, dest)

    @staticmethod
    @decor.debug
    @decor.dryrun
    def run(cmd, raiseerror=True):
        ret = os.system(f"/bin/bash -c \"set -o pipefail; {cmd}\"")
        # wenn keine bash benötigt wird:
        #ret = os.system(cmd)
        if ret != 0:
            errtext=f"run: returncode={ret}  in: {cmd}"
            logger.error(errtext)
            if raiseerror:
                raise Exception(errtext)
        return ret


init()

if __name__ == "__main__":
    # execute only if run as a script
    try:
        logger.info(f"start  cli: {cli}")
        ret = main()
        logger.info(f"stop with returncode={ret}")
        if ret > 255:
            logger.info(f"returncode > 255 return now 1")
            sys.exit(1)
        sys.exit(ret)
    except Exception as err:
        logger.error(err)
        raise(err)
else:
    pass
    

